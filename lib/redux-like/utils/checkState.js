function checkState(state, prevState) {
  return Object.entries(state).some(([key, value]) => prevState[key] !== value);
}

export default checkState;

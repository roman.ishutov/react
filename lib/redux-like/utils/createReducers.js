function createReducers(reducerObject, initState) {
  return function reducer(state = initState, action) {
    if (!Reflect.has(reducerObject, action.type)) {
      return state;
    }

    return { ...state, ...reducerObject[action.type](state, action) };
  };
}

export default createReducers;

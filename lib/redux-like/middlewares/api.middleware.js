export const API_REQUEST = Symbol('API_REQUEST');

const apiMiddleware = ({ dispatch }) => action => next => {
  if (typeof action !== 'object' || action.type !== API_REQUEST) {
    return next(action);
  }

  if (!Array.isArray(action.types)) {
    throw new Error('Action must be an array');
  }

  if (action.types.length !== 3) {
    throw new Error('Action types property must be an array of three elements');
  }

  if (action.types.every(item => typeof item !== 'string')) {
    throw new Error('Every item in action types array must be a string type');
  }

  if (typeof action.request !== 'function') {
    throw new Error('You need to put function in request property');
  }

  const [LOADING, SUCCESS, ERROR] = action.types;
  const promise = action.request();


  if (!(promise instanceof Promise)) {
    throw new Error('Action request must return a Promise');
  }

  dispatch({ type: LOADING });
  promise
    .then(data => {
      dispatch({ type: SUCCESS, data });
    })
    .catch(error => {
      dispatch({ type: ERROR, error });
    });
};

export default apiMiddleware;

/* eslint-disable no-console */

const loggerWithTimeMiddleware = store => action => next => {
  const prevState = { ...store.getState() };
  const startDate = new Date();
  const currentTime = `${
    startDate.getHours()}:${
    startDate.getMinutes()}:${
    startDate.getSeconds()}`;


  next(action);

  const timeDifference = new Date() - startDate;


  console.groupCollapsed(`action ${String(action.type)} @ ${currentTime} (in ${timeDifference / 1000} ms)`);
  console.info('%cprev state', 'color:grey; font-weight: bold', prevState);
  console.info('%caction', 'color:blue; font-weight: bold;', action);
  console.info('%cnext state', 'color: green; font-weight: bold;', JSON.parse(JSON.stringify(store.getState())));
  console.groupEnd();
};

export default loggerWithTimeMiddleware;

const thunkMiddleware = ({ dispatch }) => action => next => {
  if (typeof action === 'function') {
    action(dispatch);
    return;
  }

  next(action);
};

export default thunkMiddleware;

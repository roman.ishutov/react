import { Store } from '../';

const reducer = jest.fn((s, a) => s + 1);
const store = new Store({ reducer }, [], { reducer: 3 });

describe('tests for method subscribe', () => {
  test('instance has method subscribe', () => {
    expect(store.subscribe).toBeInstanceOf(Function);
  });

  test('should add listener to ', () => {
    const listener = jest.fn();

    store.subscribe(listener);
    store.dispatch({ type: 'test' });
    expect(listener.mock.calls.length).toBe(1);
  });

  test('a method should have one argument', () => {
    expect(store.subscribe).toHaveLength(1);
  });
});

import { Store } from '../';

const key1 = () => null;
const key2 = () => null;
const store = new Store({ key1, key2 });

describe('tests for method getState', () => {
  test('instance has method getState', () => {
    expect(store.getState).toBeInstanceOf(Function);
  });

  test('should return current state', () => {
    store.dispatch({ type: '' });
    const state = store.getState();
    expect(store.getState()).toEqual({ key1: null, key2: null });
  });

  test('a method should have no arguments', () => {
    expect(store.getState).toHaveLength(0);
  });
});

import { Store, createReducers } from '../';

const initState = {
  data: [],
  loading: false,
  selected: null
};

const list = createReducers(
  {
    'list.request': (state, action) => ({ ...state, loading: true }),
    'select.list.item': (state, action) => ({ ...state, selected: action.payload }),
    'load.list.success': (state, action) => ({ ...state, loading: false, data: action.payload }),
    'change.state': (state, action) => ({ ...state, loading: true, selected: 1 })
  },
  initState
);

const count = createReducers(
  {
    inc: (state, action) => state + action.payload,
    dec: (state, action) => state - action.payload,
    'change.state': (state, action) => state + 1
  },
  0
);

const store = new Store({ list, count });

test('should change as many subStates as reducers have the same action type', () => {
  const oldState = Object.assign({}, store.getState());
  store.dispatch({ type: 'change.state' });
  const newState = store.getState();

  expect(newState).not.toEqual(oldState);
  expect(newState.count).toBe(1);
  expect(newState.list.loading).toBeTruthy();
  expect(newState.list.selected).toBe(1);
});

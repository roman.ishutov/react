import { Store, createReducers } from '../';

const reducer = jest.fn((s, a) => s + 1);
const store = new Store({ reducer }, [], { reducer: 3 });

describe('tests for method dispatch', () => {
  test('instance has method dispatch', () => {
    expect(store.dispatch).toBeInstanceOf(Function);
  });

  test('should call each reducer one time ', () => {
    const cbList = jest.fn();
    const cbCount = jest.fn();

    const store = new Store({ cbList, cbCount });
    store.dispatch({ type: 'test' });
    expect(cbList.mock.calls.length).toBe(2);
    expect(cbCount.mock.calls.length).toBe(2);
  });

  test('should call each listener one time ', () => {
    const listener1 = jest.fn();
    const listener2 = jest.fn();

    store.subscribe(listener1);
    store.subscribe(listener2);

    store.dispatch({ type: 'test' });
    expect(listener1.mock.calls.length).toBe(1);
    expect(listener2.mock.calls.length).toBe(1);
  });

  test('should call a listener with current state', () => {
    const cbList = jest.fn();
    const cbCount = jest.fn();
    const store = new Store({ cbList, cbCount });

    const listener1 = jest.fn();
    store.subscribe(listener1);
    store.dispatch({ type: 'test' });

    expect(listener1.mock.calls[0][0]).toBe(store.getState());
  });

  test('should not call a listener if it added after call dispatch', () => {
    const listener1 = jest.fn();
    const listener2 = jest.fn();

    store.subscribe(listener1);
    store.dispatch({ type: 'test' });
    store.subscribe(listener2);

    expect(listener1.mock.calls.length).toBe(1);
    expect(listener2.mock.calls.length).toBe(0);
  });

  test('a method should have one argument', () => {
    expect(store.dispatch).toHaveLength(1);
  });

  test('shouldn\'t call listeners if state didn\'t change', () => {
    const initState = { user: {}, count: 1 };
    const reducerObj = { 'counter.increment': (state, action) => state + 1 };
    const count = createReducers(reducerObj);
    const listener1 = jest.fn();
    const listener2 = jest.fn();
    const store = new Store({ count }, [], initState);

    store.subscribe(listener1);
    store.subscribe(listener2);
    store.dispatch({ type: 'counter.decrement' });

    expect(store.getState()).toEqual(initState);
    expect(listener1.mock.calls.length).toBe(0);
    expect(listener2.mock.calls.length).toBe(0);
  });

  test('should call listeners if state changed', () => {
    const initState = { user: {}, count: 1 };
    const newState = { user: {}, count: 2 };
    const reducerObj = { 'counter.increment': (state, action) => state + 1 };
    const count = createReducers(reducerObj);
    const listener1 = jest.fn();
    const listener2 = jest.fn();
    const store = new Store({ count }, [], initState);

    store.subscribe(listener1);
    store.subscribe(listener2);
    store.dispatch({ type: 'counter.increment' });

    expect(store.getState()).toEqual(newState);
    expect(listener1.mock.calls.length).toBe(1);
    expect(listener2.mock.calls.length).toBe(1);
  });
});

import { Store } from '../';

describe('tests for method middleware', () => {
  test('Reducers and subscribers should be called after middlewares function', () => {
    const reducer = jest.fn((s, a) => s + 1);
    const subscriber = jest.fn();
    const action = { type: reducer.name };

    const fn = jest.fn(a => n => n(a));

    const store = new Store({ reducer }, [s => fn, s => fn, s => a => n => n(action)], { reducer: 1 });

    store.subscribe(subscriber);
    store.dispatch({ type: 'reducer' });

    expect(fn.mock.calls).toHaveLength(2);
    expect(reducer.mock.calls[1][1]).toBe(action);
    expect(subscriber.mock.calls[0][0]['reducer']).toBe(3);
  });

  test('Every middleware must be called only after next function', () => {
    const action = { type: 'test' };
    const fn = jest.fn(a => n => n(a));
    const fnWithoutNext = jest.fn(a => n => null);

    const store = new Store({}, [s => fn, s => fn, s => fnWithoutNext, s => fn]);
    store.dispatch(action);

    expect(fn.mock.calls[0][0]).toBe(action);
    expect(fn.mock.calls[1][0]).toBe(action);
    expect(fnWithoutNext.mock.calls[0][0]).toBe(action);
    expect(fn.mock.calls).toHaveLength(2);
  });

  test('Next middlewares is not called if the type of action is missing', () => {
    const fn = jest.fn(a => n => n(a));
    const fnWithoutNextAction = jest.fn(a => n => n());

    const store = new Store({}, [s => fn, s => fn, s => fnWithoutNextAction, s => fn]);

    store.dispatch({});
    expect(fn).not.toHaveBeenCalled();
    expect(fnWithoutNextAction).not.toHaveBeenCalled();

    store.dispatch({ type: 'test' });
    expect(fn.mock.calls).toHaveLength(2);
    expect(fnWithoutNextAction.mock.calls).toHaveLength(1);
  });

  test('Every middleware must be called one by one, providing action to next-function', () => {
    const action = { type: 'inc', k: 0 };
    const fn = jest.fn(({ type, k }) => n => n({ type, k: k + 1 }));
    const reducerList = jest.fn();

    const store = new Store({ reducerList }, [s => fn, s => fn, s => fn, s => fn]);
    store.dispatch(action);

    expect(fn.mock.calls[0][0].k).toBe(0);
    expect(fn.mock.calls[1][0].k).toBe(1);
    expect(fn.mock.calls[2][0].k).toBe(2);
    expect(fn.mock.calls[3][0].k).toBe(3);
    expect(reducerList.mock.calls[1][1].k).toBe(4);
  });

  test('reducer should take value from lastest middleware', () => {
    const fn = jest.fn(a => n => n({ type: 'last' }));
    const md = s => a => n => n(a);
    const reducer = jest.fn();

    const store = new Store({ reducer }, [md, md, md, s => fn]);
    store.dispatch({ type: 'init' });

    expect(fn.mock.calls[0][0].type).toBe('init');
    expect(reducer.mock.calls[1][1].type).toBe('last');
  });

  test('Correctly works with 1 middleware', () => {
    const action = { type: 'correct' };
    const reducer = jest.fn();
    const fn = jest.fn(a => n => n(action));

    const store = new Store({ reducer }, [s => fn]);
    store.dispatch({ type: 'test' });

    expect(reducer.mock.calls).toHaveLength(2);
    expect(reducer.mock.calls[1][1]).toBe(action);
  });

  test('Correctly works with 0 middleware', () => {
    const action = { type: 'correct' };
    const reducer = jest.fn();

    const store = new Store({ reducer });
    store.dispatch(action);

    expect(reducer.mock.calls).toHaveLength(2);
    expect(reducer.mock.calls[1][1]).toBe(action);
  });

  test('Dispatch inside middleware should give control to the beginning', () => {
    const action = { type: 'second' };
    const reducer = jest.fn();
    const md = jest.fn(a => n => n(a));
    const fn = jest.fn(s => a => n => {
      if (a.type === 'first') {
        return s.dispatch(action);
      }
      n(a);
    });

    const store = new Store({ reducer }, [s => md, fn, s => md, s => md]);
    store.dispatch({ type: 'first' });

    expect(md.mock.calls[0][0].type).toBe('first');
    expect(md.mock.calls[1][0].type).toBe('second');
    expect(md.mock.calls[3][0].type).toBe('second');
    expect(md.mock.calls).toHaveLength(4);
  });

  test('Middlewares has access to store, action inside function body', () => {
    const fnWithNext = jest.fn(n => null);
    const fnWithAction = jest.fn(a => n => n(a));
    const fnWithStore = jest.fn(s => a => n => n(a));

    const store = new Store({}, [fnWithStore, s => fnWithAction, s => a => fnWithNext]);

    const action = { type: 'inc' };
    store.dispatch(action);

    expect(fnWithStore.mock.calls[0][0]).toBe(store);
    expect(fnWithAction.mock.calls[0][0]).toBe(action);
    expect(typeof fnWithNext.mock.calls[0][0]).toBe('function');
  });
});

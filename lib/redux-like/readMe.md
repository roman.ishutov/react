# Redux-like

This is redux-like library which connect your store which your components

This library contain store which call middleWares after that dispatch actions in reducers

Middlewares are function which make a side effects in reducers

Store contain methods
* dispatch - the dispatch function takes an object as input and the only one, but mandatory, is the type field. Actually, dispatch is an action, that is, an event, and type is the type of this event.
* getState - Returns the current state of your application. It is equal to the last returned value from the store reducer.
* subscribe - Adds a listener. It will be called every time an action is dispatched and some part of the state tree could potentially change. You can then call getState() to read the current state of the store tree inside the callback.

# Example of usage this library
```js
const counter = {
  'counter.increment': (state, action) => ({ ...state, value: state.value + 1 })
};

const counterReducers = utils.createReducers(counter);

const store = new Store({ count: counterReducers}, [], { value: 0});


console.log(store.getState()); // { count: { value: 0 } }
store.dispatch({ type: 'counter.increment' });
console.log(store.getState()); // { count: { value: 1 } }


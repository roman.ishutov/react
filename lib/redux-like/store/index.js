import checkState from '../utils/checkState';

class Store {
  #callReducers = action => {
    for (const key in this.reducers) {
      this.state[key] = this.reducers[key](this.state[key], action);
    }
  };

  #callMiddlewares = action => {
    let md = [...this.middleWares];
    const next = action => {
      const tempMd = [...md];


      if (!action) {
        return;
      }

      if (md.length) {
        md.shift()(this)(action)(next);
        md = tempMd;
        return;
      }

      this.#callReducers(action);
    };
    next(action);
  };

  constructor(reducers = {}, middleWares = [], initialState = {}) {
    this.state = initialState;
    this.listeners = [];
    this.reducers = reducers;
    this.middleWares = middleWares;
    this.dispatch = this.dispatch.bind(this);
    this.#callReducers({});
  }

  dispatch(action) {
    if (!action.type) {
      return;
    }

    const prevState = { ...this.state };


    this.#callMiddlewares(action);

    if (!checkState(this.state, prevState)) {
      return;
    }

    this.listeners.forEach(listener => listener(this.state));
  }

  subscribe(listener) {
    if (typeof listener !== 'function') {
      return;
    }

    this.listeners.push(listener);
  }

  getState() {
    return this.state;
  }
}


export default Store;

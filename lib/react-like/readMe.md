# React-like

This is react-like library which contains BaseComponent for your react-like component.

## Example of usage this library
```js
import BaseComponent from 'react-like';

class List extends BaseComponent {

}

const { body } = document;

list = new List({ data: ['AAAAA'] }, body);
```

And you will see your react-like component in your browser.

Also you can change props and you will see that they are changed in browser.

But you need to do that immutably.

# You can use lifecycle methods.

```render```
 ```
 The render() function must be pure

 render() is not called if shouldComponentUpdate() returns false.

 Can't call 2 renders or more

```
```componentWillMount()```
```
is called just before mounting.
```
```componentDidUpdate()```

```
 is called right after the update. Not called on first render.

The method allows you to work with the DOM when updating the component
```
```componentWillReceiveProps()```
```
 is called before the mounted component receives new props. To update state in response to prop changes
```
```shouldComponentUpdate()```
```
to indicate the need for the next render based on state changes and props. The default is to re-render on any state change.
```
```componentWillUpdate()```
```
is called just before rendering when new props or state are received. In this method, you can do some pre-upgrade preparation. This method is not called on the first render.
```
``` componentDidUpdate()```
```
is called right after the update. Not called on first render.

The method allows you to work with the DOM when updating the component.
 ```
```componentWillUnmount()```
```
 is called just before the component is unmounted and removed. This method does the necessary reset: canceling timers, network requests, and subscriptions created in componentDidMount().
```
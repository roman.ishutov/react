/* eslint-disable no-console */
function checkPropTypes(incomingPropTypes, component) {
  function getArrayTypeName(value) {
    return value.slice(6, -1);
  }

  const { propTypes } = component.constructor;


  for (const key in incomingPropTypes) {
    const incomingPropType = incomingPropTypes[key];
    const PropType = propTypes[key];

    if ((/Array<.*>/g).test(PropType)) {
      const elementsType = getArrayTypeName(PropType);


      if (!Array.isArray(incomingPropType)) {
        console.warn(`Property: ${key} is not Array`);
      } else if (!incomingPropType.every(item => typeof item === elementsType)) {
        console.warn(`Property: ${key} has wrong type of elements`);
      }
    } else if (typeof incomingPropType !== PropType) {
      console.warn(`${key} is not a valid type`);
    }
  }
}

export default checkPropTypes;

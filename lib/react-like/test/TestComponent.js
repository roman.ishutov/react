import BaseComponent from '../core/index';

const mocks = {
  render: jest.fn(function() { return this.props; }),
  componentWillMount: jest.fn(function() { return this.props; }),
  componentDidMount: jest.fn(function() { return this.props; }),
  componentWillReceiveProps: jest.fn(function(nextProps) { return this.props; }),
  componentShouldUpdate: jest.fn(function(nextProps) { return true; }),
  componentWillUpdate: jest.fn(function() { return this.props; }),
  componentDidUpdate: jest.fn(function(prevProps) { return this.props; }),
  componentWillUnmount: jest.fn(function() { return this.props; })
};

global.MutationObserver = class {
  constructor(callback) {} // eslint-disable-line no-useless-constructor
  disconnect() {}
  observe(node, config) {}
};

export default class TestComponent extends BaseComponent {
  constructor(props, node) {
    Object.values(mocks).forEach(mockFn => mockFn.mockClear());
    super(props, node);
    this.mocks = mocks;
  }

  componentWillMount() {
    mocks.componentWillMount.call(this);
  }

  componentDidMount() {
    mocks.componentDidMount.call(this);
  }

  render() {
    mocks.render.call(this);
  }

  componentWillReceiveProps(nextProps) {
    mocks.componentWillReceiveProps.call(this, nextProps);
  }

  componentShouldUpdate(nextProps) {
    return mocks.componentShouldUpdate(nextProps);
  }

  componentWillUpdate() {
    mocks.componentWillUpdate.call(this);
  }

  componentDidUpdate(prevProps) {
    mocks.componentDidUpdate.call(this, prevProps);
  }

  componentWillUnmount() {
    mocks.componentWillUnmount.call(this);
  }
}

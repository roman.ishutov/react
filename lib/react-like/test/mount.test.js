import TestComponent from './TestComponent.js';

const props = {
  name: 'Vadick',
  lastName: 'Trocenkin'
};

const node = document.createElement('div');

describe('Tests for mount stage', () => {
  const { mocks } = new TestComponent(props, node);

  test('1.1 Method componentWillMount should accept 0 arguments', () => {
    expect(mocks.componentWillMount.mock.calls[0]).toHaveLength(0);
  });

  test('1.4 Method componentWillMount should have actual props', () => {
    expect(mocks.componentWillMount).toReturnWith(props);
  });

  test('1.3 Method componentDidMount should accept 0 arguments', () => {
    expect(mocks.componentDidMount.mock.calls[0]).toHaveLength(0);
  });

  test('1.6 Method componentDidMount should have actual props', () => {
    expect(mocks.componentDidMount).toReturnWith(props);
  });

  test('1.5 Method render should have actual props', () => {
    expect(mocks.render).toReturnWith(props);
  });

  test('1.2 Method render should accept 0 arguments', () => {
    expect(mocks.render.mock.calls[0]).toHaveLength(0);
  });
});

describe('2 Test for lifecycle mount', () => {
  const { mocks } = new TestComponent(props, node);

  test('2.1 Lifecycle mount', () => {
    expect(mocks.componentWillMount).toHaveBeenCalledTimes(1);
    expect(mocks.render).toHaveBeenCalledTimes(1);
    expect(mocks.componentDidMount).toHaveBeenCalledTimes(1);
    expect(mocks.componentWillReceiveProps).not.toBeCalled();
    expect(mocks.componentShouldUpdate).not.toBeCalled();
    expect(mocks.componentWillUpdate).not.toBeCalled();
    expect(mocks.componentDidUpdate).not.toBeCalled();
    expect(mocks.componentWillUnmount).not.toBeCalled();
    expect(mocks.componentWillMount.mock.invocationCallOrder[0])
      .toBeLessThan(mocks.render.mock.invocationCallOrder[0]);
    expect(mocks.render.mock.invocationCallOrder[0])
      .toBeLessThan(mocks.componentDidMount.mock.invocationCallOrder[0]);
  });
});

import TestComponent from './TestComponent';

const props = {
  name: 'Vadick',
  lastName: 'Trocenkin'
};

const nextProps = {
  name: 'Egor',
  lastName: 'Guzenkins'
};
const node = document.createElement('div');

describe('1 Tests for update ', () => {
  const inst = new TestComponent(props, node);
  const { mocks } = inst;

  inst.props = nextProps;

  test('1.1 Method componentWillReceiveProps should accept 1 arguments', () => {
    expect(mocks.componentWillReceiveProps.mock.calls[0]).toHaveLength(1);
  });

  test('1.2 Method componentWillReceiveProps should accept next props', () => {
    expect(mocks.componentWillReceiveProps).toBeCalledWith(nextProps);
  });

  test('1.3 Method componentWillReceiveProps should has old props in this', () => {
    expect(mocks.componentWillReceiveProps).toReturnWith(props);
  });

  test('1.4 Method componentShouldUpdate should accept 1 arguments', () => {
    expect(mocks.componentShouldUpdate.mock.calls[0]).toHaveLength(1);
  });

  test('1.5 Method componentShouldUpdate should accept next props', () => {
    expect(mocks.componentShouldUpdate).toBeCalledWith(nextProps);
  });

  test('1.6 Method componentShouldUpdate should return boolean type', () => {
    expect(mocks.componentShouldUpdate).toReturnWith(true);
  });

  test('1.7 Method componentWillUpdate should has new props in this', () => {
    expect(mocks.componentWillUpdate).toReturnWith(nextProps);
  });

  test('1.8 Method componentWillUpdate should accept 0 arguments', () => {
    expect(mocks.componentWillUpdate.mock.calls[0]).toHaveLength(0);
  });

  test('1.9 Method componentDidUpdate should accept prev props', () => {
    expect(mocks.componentDidUpdate).toBeCalledWith(props);
  });

  test('1.10 Method componentDidUpdate should accept 1 arguments', () => {
    expect(mocks.componentDidUpdate.mock.calls[0]).toHaveLength(1);
  });

  test('1.11 Method componentDidUpdate should has new props in this', () => {
    expect(mocks.componentDidUpdate).toReturnWith(nextProps);
  });

  test('1.12 Method render should has new props in this', () => {
    expect(mocks.render).toReturnWith(nextProps);
  });
});

describe('2 Test for lifecycle ', () => {
  test('2.1 Lifecycle update ', () => {
    const inst = new TestComponent(props, node);
    const { mocks } = inst;
    Object.values(mocks).forEach(fn => fn.mockClear());
    inst.props = nextProps;

    expect(mocks.componentWillReceiveProps).toHaveBeenCalledTimes(1);
    expect(mocks.componentShouldUpdate).toHaveBeenCalledTimes(1);
    expect(mocks.componentWillUpdate).toHaveBeenCalledTimes(1);
    expect(mocks.render).toHaveBeenCalledTimes(1);
    expect(mocks.componentDidUpdate).toHaveBeenCalledTimes(1);
    expect(mocks.componentWillReceiveProps.mock.invocationCallOrder[0])
      .toBeLessThan(mocks.componentShouldUpdate.mock.invocationCallOrder[0]);
    expect(mocks.componentShouldUpdate.mock.invocationCallOrder[0])
      .toBeLessThan(mocks.componentWillUpdate.mock.invocationCallOrder[0]);
    expect(mocks.componentWillUpdate.mock.invocationCallOrder[0])
      .toBeLessThan(mocks.render.mock.invocationCallOrder[0]);
    expect(mocks.render.mock.invocationCallOrder[0])
      .toBeLessThan(mocks.componentDidUpdate.mock.invocationCallOrder[0]);
  });
});

/* eslint-disable class-methods-use-this */
/* eslint-disable no-empty-function */
import { checkPropTypes } from '../utils/index';

class BaseComponent {
  constructor(props, node) {
    this.props = props;
    this.node = node;
    this.componentWillMount();
    checkPropTypes(props, this);
    const observer = new MutationObserver(() => {
      if (!this.node.isConnected) {
        this.componentWillUnmount();
        observer.disconnect();
      }
    });

    observer.observe(this.node.parentNode, {
      childList: true
    });

    this.render();
    this.componentDidMount();

    return new Proxy(this, {
      set(target, prop, value) {
        if (prop !== 'props') {
          target[prop] = value;

          return true;
        }

        const oldProps = target.props;
        const newProps = { ...oldProps, ...value };


        checkPropTypes(newProps, target);
        target.componentWillReceiveProps(newProps);

        if (!target.componentShouldUpdate(newProps)) {
          return true;
        }

        target.props = newProps;
        target.componentWillUpdate();
        target.render();
        target.componentDidUpdate(oldProps);

        return true;
      }
    });
  }
  componentWillMount() {}

  componentWillReceiveProps(nextProps) {}

  componentWillUpdate() {}

  componentDidUpdate(prevProps) {}

  componentDidMount() {}

  componentShouldUpdate(nextProps) {
    return true;
  }

  componentWillUnmount() {}
  render() {
    throw new Error('obligatory to implementation');
  }
}

export default BaseComponent;

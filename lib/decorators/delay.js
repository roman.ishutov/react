export default function delay(ms) {
  return function(target, key, descriptor) {
    const fn = descriptor.value;


    descriptor.value = function(...args) {
      setTimeout(() => fn.apply(this, args), ms);
    };
  };
}



# readOnly

This is decorator which allow you to set property to be read only.

```js
class ReactComponent {
  @readOnly
  prop = 'Vadik';

  render() {
    this.prop = 'Egot'; // error
  }
}
```

# toUpperCase

This is decorator which allow you to set property to be uppercase.

```js
class ReactComponent {
  @toUpperCase
  prop = 'egot';

  render() {
    console.log(this.prop); // EGOT
  }
}
```
# delay


decorator creates settimeout

```js
class ReactComponent {
  @timeout(2000)
  doAsync(fn) {
    fn();
  }

  loadListOfFiles() {
    console.log(this); // reactComponent { props: {} }
  }

  componentDidMount() {
    this.doAsync(this.loadListOfFiles);
  }
}

```

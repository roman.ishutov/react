export default function readOnly() {
  return function(target, key, descriptor) {
    descriptor.writable = false;
  };
}

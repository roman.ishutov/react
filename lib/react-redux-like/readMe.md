# React-redux-like

React-redux-like library which connect your store with your components.

Have decorator `connect` which connect your store with your components.

Also have:

* mapStateToProps - return props for certain component from state
* mapDispatchToProps - can work in two ways:
  * if function - return object with added methods for props
  * if object - return object with added methods for props

## Example of usage this library
```js
const mapStateToProps = ({ component }) => ({ component });

const mapDispatchToProps = {
//pass functions or object(function) to component
};

@connect(mapStateToProps, mapDispatchToProps)
class Component extends BaseComponent {

  componentDidMount() {
  // call the passed function
  }

  render() {
    //render component
  }
}

component = new Component({ props }, body); // render AAAAA

```

And you will see your react-like component in your browser.

And after 1 second you will see that list is changed.

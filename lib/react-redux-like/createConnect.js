export default function createConnect(store) {
  return function(mapStateToProps, mapDispatchToProps) {
    return function(Component) {
      return function(props, node) {
        const allProps = Object.assign(mapStateToProps && mapStateToProps(store.getState()) || {}, props);


        if (typeof mapDispatchToProps === 'function') {
          Object.assign(allProps, mapDispatchToProps(store.dispatch));
        } else if (!mapDispatchToProps || !Object.keys(mapDispatchToProps).length) {
          Object.assign(allProps, { dispatch: store.dispatch });
        } else {
          for (const key in mapDispatchToProps) {
            allProps[key] = (...args) => store.dispatch(mapDispatchToProps[key](...args));
          }
        }

        const component = new Component(allProps, node);


        store.subscribe(state => {
          const newProps = mapStateToProps(state);


          for (const key in component.props) {
            if (newProps[key] !== component.props.key) {
              component.props = newProps;
              return;
            }
          }
        });

        return component;
      };
    };
  };
}
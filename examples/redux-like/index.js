/* eslint-disable no-console */
const counter = {
  'counter.increment': (state, action) => ({ ...state, value: state.value + 1 })
};

const counterReducers = utils.createReducers(counter);

const store = new Store({ count: counterReducers }, [], { value: 0 });


console.log(store.getState()); // { count: { value: 0 } } }
store.dispatch({ type: 'counter.increment' });
console.log(store.getState()); // { count: { value: 1 } } }

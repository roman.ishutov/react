/* eslint-disable no-undef */
const mapStateToProps = ({ list }) => ({ ...list });
const mapDispatchToProps = {
  loadListOfFiles

};


connect(mapStateToProps, mapDispatchToProps);
class List extends BaseComponent {
  static propTypes = {
    data: 'Array<string>',
    loadListOfFiles: 'function',
    error: 'string',
    isLoading: 'boolean'
  };


  constructor(...props) {
    super(...props);
    console.log(0, 'constructor'); // eslint-disable-line no-console
  }

  componentWillMount() {
    console.log(1, 'componentWillMount'); // eslint-disable-line no-console
  }
  componentDidMount() {
    console.log(2, 'componentDidMount'); // eslint-disable-line no-console
    setTimeout(() => {
      this.props.loadListOfFiles(); }, 1000);
  }

  render() {
    const html = this.props.data.map(item => `<li>${item}</li>`);
    this.node.innerHTML = `<ul>${html}</ul>`;
  }
}

const { body } = document;


window.list = new List({ data: ['Vadick', 'Danilo', 'Timofey'] }, body); // render AAAAA

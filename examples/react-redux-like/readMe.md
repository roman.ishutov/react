This is a example of react-redux-like library
We created two functions:

mapStateToProps - return props for certain component from state
mapDispatchToProps - return methods

After that called connect decorator which connect store with component. And call dispatch method which dispatch action in reducers and do some action in middleWares connected with this action.
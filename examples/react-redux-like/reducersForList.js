const initState = {
  data: ['Vadick', 'Danilo', 'Timofey'],
  isLoading: false,
  error: ''
};

const reducerOfList = {
  'LIST_LOADING': (state, action) => ({ ...state, isLoading: true }),
  'LIST_SUCCESS': (state, action) => ({ ...state, data: action.data, isLoading: false }),
  'LIST_ERROR': (state, action) => ({ ...state, error: action.error, isLoading: false })
};

// eslint-disable-next-line no-undef
const list = createReducers(reducerOfList, initState);

const loadListApi = Promise.resolve(['Vlad', 'Victoriya', 'Sergey', 'Volodya']);
const API_REQUEST = 'API_REQUEST';
const LIST_LOADING = 'LIST_LOADING';
const LIST_SUCCESS = 'LIST_SUCCESS';
const LIST_ERROR = 'LIST_ERROR';

const loadListOfFiles = () => ({
  type: API_REQUEST,
  request: () => loadListApi,
  types: [LIST_LOADING, LIST_SUCCESS, LIST_ERROR]
});

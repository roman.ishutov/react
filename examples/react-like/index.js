import { BaseComponent } from '@react-like';

class List extends BaseComponent {
  static propTypes = {
    data: 'Array<string>',
    loadListOfFiles: 'function'
  };

  constructor(...props) {
    super(...props);
    console.log(0, 'constructor'); // eslint-disable-line no-console
  }

  componentWillMount() {
    console.log(1, 'componentWillMount'); // eslint-disable-line no-console
  }

  componentDidMount() {
    this.props.loadListOfFiles();
    console.log(2, 'componentDidMount'); // eslint-disable-line no-console
  }

  render() {
    const html = this.props.data.map(item => `<li>${item}</li>`);
    this.node.innerHTML = `<ul>${html}</ul>`;
  }
}

const { body } = document;

function loadListOfFiles() {
  setTimeout(() => {
    window.list.props = {
      ...window.list.props,
      data: ['AAAAAA', 'BBBBB']
    };
  }, 2000); // render 'AAAAAA', 'BBBBB'
}

window.list = new List({ data: ['AAAAA'], loadListOfFiles }, body); // render AAAAA

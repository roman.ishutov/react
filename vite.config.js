import path from 'path';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

const mode = process.env.NODE_ENV;
const isProd = mode === 'production';
const config = {
  mode,
  build: {
    outDir: path.resolve(__dirname, 'dist'),
    sourcemap: isProd,
    emptyOutDir: isProd
  },
  resolve: {
    alias: {
      '@react-like': path.resolve(__dirname, 'lib/react-like'),
      '@redux-like': path.resolve(__dirname, 'lib/redux-like'),
      '@react-redux': path.resolve(__dirname, 'lib/react-redux-like')
    }
  },
  plugins: [
    react({
      babel: {
        plugins: [
          ['@babel/plugin-proposal-decorators', { 'legacy': true }],
          ['@babel/plugin-proposal-class-properties']
        ]
      }
    })
  ]
};

export default defineConfig(config);
